/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.View;
import model.Board;
import view.BoardView;
import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class ConnectN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int op=1;
        Board b=new Board();
        Strategy s=new Strategy(b);
        BoardView bv=new BoardView(b);
        while (op!=0) {
            op=View.mainMenu();
            switch (op) {
                case 1:
                    playGame(b, s, bv);
                    break;
                case 2:
                    settings(b);
                    break;
                case 3:
                    break;
                case 4:
                    break;                
            }
        }
    }
    
    static void playGame(Board b, Strategy s, BoardView bv) {
        int movement;
        b.resetBoard();
        boolean exit=false;
        //White start
        boolean whiteTurn=true; 
        bv.showBoard();
        while (!exit) {
            if (whiteTurn) {
                movement=View.testInteger("Enter col: ", 1, b.getCols());
                b.move(true, movement);
                whiteTurn=false;
            } else {
                View.pause();
                b.move(false, s.selectMoveStrategy1(b));
                whiteTurn=true;
            }
            bv.showBoard();            
            if (s.testWin(b)) {
                exit=true;
            }
        }
    }
    
    static void settings(Board b) {
        int op2=1;
        while (op2!=0) {
            op2=View.settingsMenu(b);
            switch(op2) {
                case 1:
                    setDimensions(b);
                    break;
                case 2:
                    setColor(b);
                    break;
                case 3:
                    setN(b);
                    break;
            }
        }
    }
    
    static void setDimensions(Board b) {
        b.setRows(View.testInteger("Rows (5-25):",5,25));
        b.setCols(View.testInteger("Cols (5-25):",5,25));
    }
    
    static void setN(Board b) {
        b.setN(View.testInteger("Number of tokens connected to win (3-10): ", 3, 10));
    }
    
    static void setColor(Board b) {
        b.setHumanColor();
    }
}
