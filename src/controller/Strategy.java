package controller;

import model.Board;

/**
 *
 * @author nacho
 */
public class Strategy {
    
    Board b;
    int moves[];
    
    public Strategy(Board b) {
        this.b=b;
        this.moves=new int[b.getCols()+1];
    }
    
    public int selectMoveRandom() {
        return (int) (Math.random() * b.getCols() + 1);
    }
    
    public int selectMoveStrategy1(Board b) {
        int candidate;
        // At first time we look for a winner movement
        candidate=seekWinnerMovement(b, -1);
        if (candidate!=0) return candidate;
        // If not exists then look for a opponent winner movement
        candidate=seekWinnerMovement(b, 1);
        if (candidate!=0) return candidate;
        // Else computer does a random movement
        return selectMoveRandom();
    }
    
    private int seekWinnerMovement(Board b, int color) {
        int candidate=1;
        boolean moveWinner=false;

        while (!moveWinner && candidate<=moves.length-1) {
            if (seekWin(candidate, b, color)) {
                moveWinner=true;
                break;
            }
            candidate++;
        }
        
        if (moveWinner) {
            return candidate;
        } else {
            return 0;
        }
    }
    
    private boolean seekWin(int candidate, Board b, int color) {
        int busyRows=b.getElement(0, candidate);
        int rowCandidate=b.getRows()-busyRows;
        return testWin(rowCandidate, candidate, b, color);
    }
    
    private boolean testWin(int row, int col, Board b, int color) {
        int counter;
        // Test Vertically
        if (row<=b.getRows()-b.getN()+1) { //if so close bottom there aren't space
            if (countVertically(row, col, b, color)>=b.getN()) return true;
        }
        // Test Horizontally
        // Left First
        if (col>1) {
            counter=countLeftHorizontally(row, col, b, color);
            if (counter>=b.getN()) return true;
        } else {
            counter=1;
        }
        // Right
        if (col<b.getRows()) {
            if (countRightHorizontally(row, col, b, color)+counter>=b.getN()) return true;
        }
        // Test diagonals
        // Top Left Down Right Diagonal
        if (countTopLeftDownRightDiagonal(row, col, b, color)>=b.getN()) return true;
        // Down Left Top Right Diagonal
        return (countDownLeftTopRightDiagonal(row, col, b, color)>=b.getN());
    }
    
    public boolean testWin(Board b) {
        return testWin(b.getLastRow(), b.getLastCol(), b, b.getColor());
    }
    
    private int countVertically(int row, int col, Board b, int color) {
        int counter=1;
        for (int i = row+1; i <= b.getRows(); i++) {
            if (b.getElement(i, col)==color) {
                counter++;
            } else {
                break;
            }
        }
        return counter;
    }
    
    private int countLeftHorizontally(int row, int col, Board b, int color) {
        int counter=1;
        for (int i = col-1; i >=1; i--) {
            if (b.getElement(row, i)==color) {
                counter++;
            } else {
                break;
            }                
        }
        return counter;
    }
    
    private int countRightHorizontally(int row, int col, Board b, int color) {
        int counter=0;
        for (int i = col+1; i <=b.getRows(); i++) {
            if (b.getElement(row, i)==color) {
                counter++;
            } else {
                break;
            }                
        }            
        return counter;
    }
    
    private int countTopLeftDownRightDiagonal(int row, int col, Board b, int color) {
        int counter=1;
        int colTmp=col-1;
        int rowTmp=row-1;
        
        while (colTmp>=1 && rowTmp>=1) {
            if (b.getElement(rowTmp, colTmp)==color) {
                counter++;
                colTmp--;
                rowTmp--;
            } else {
                break;
            }
        }
        if (counter>=b.getN()) return counter;
        
        colTmp=col+1;
        rowTmp=row+1;        
        
        while (colTmp<=b.getCols() && rowTmp<=b.getRows()) {
            if (b.getElement(rowTmp, colTmp)==color) {
                counter++;
                colTmp++;
                rowTmp++;
            } else {
                break;
            }
        }        
        return counter;
    }
    
    private int countDownLeftTopRightDiagonal(int row, int col, Board b, int color) {
        int counter=1;
        int colTmp=col-1;
        int rowTmp=row+1;
        while (colTmp>=1 && rowTmp<=b.getRows()) {
            if (b.getElement(rowTmp, colTmp)==color) {
                counter++;
                colTmp--;
                rowTmp++;
            } else {
                break;
            }
        }
        if (counter>=b.getN()) return counter;
        
        colTmp=col+1;
        rowTmp=row-1;
        while (colTmp<=b.getCols() && rowTmp>=1) {
            if (b.getElement(rowTmp, colTmp)==color) {
                counter++;
                colTmp++;
                rowTmp--;
            } else {
                break;
            }
        }        
        
        return counter;
    }
}
