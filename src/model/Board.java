package model;

import tools.Constants.Color;

/**
 *
 * @author nacho
 */
public class Board {
    
    // Dimensions
    private int rows=6, cols=7;
    
    // Number of tokens to connect to win
    private int N=4;
    
    private int[][] board;
    
    private int lastRow, lastCol; //last move
    private int color;
    private Color HumanColor=Color.WHITE;
    
    public Board(){
        board=new int[rows+1][cols+1];
    }
    
    public void resetBoard() {
        board=new int[rows+1][cols+1];
    }

    public Color getHumanColor() {
        return HumanColor;
    }

    public void setHumanColor() {
        if (HumanColor==Color.WHITE) {
            HumanColor=Color.BLACK;
        } else {
            HumanColor=Color.WHITE;
        }
    }

    public int getElement(int row, int col) {
        return board[row][col];
    }
    
    public void setElement(int row, int col, Color color) {
        if (color==Color.WHITE) {
            board[row][col]=1;
        } else {
            board[row][col]=-1;
        }
    }
    
    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public int getN() {
        return this.N;
    }
    
    public void setN(int N) {
        this.N=N;
    }

    public int getLastRow() {
        return lastRow;
    }

    public int getLastCol() {
        return lastCol;
    }

    public int getColor() {
        return color;
    }

    public void move(boolean white, int col) {
        int row=rows-board[0][col];
        if (white) {
            board[row][col]=1;
            color=1;
        } else {
            board[row][col]=-1;
            color=-1;
        }
        lastRow=row;
        lastCol=col;
        board[0][col]++;
    }
}
