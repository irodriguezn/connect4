package tools;

/**
 *
 * @author nacho
 */
public class Constants {

    public enum Color {
        WHITE, BLACK;
    }
    
    public static final char WHITE_CHAR='O';
    public static final char BLACK_CHAR='X';
    public static char BLANK_CHAR='-';
}
