package view;

import java.util.Scanner;
import model.Board;

/**
 *
 * @author nacho
 */
public class View {
    
    public static int mainMenu() {
        int op;
        System.out.println("");
        System.out.println("MAIN MENU");
        System.out.println("---------");
        System.out.println("1- New Game");
        System.out.println("2- Settings");
        System.out.println("0- Exit");
        op=testInteger("Select Option (0-2): ", 0, 2);
        return op;
    }
    
    public static int settingsMenu(Board b) {
        int op;
        System.out.println("");
        System.out.println("SETTINGS");
        System.out.println("---------");
        System.out.println("1- Change Dimensions - Rows: " + b.getRows() + " Cols: " + b.getCols());
        System.out.println("2- Human color: " + b.getHumanColor());
        System.out.println("3- Number of tokens to connect to win: " + b.getN());
        System.out.println("0- Main Menu");
        op=View.testInteger("Select option (0-3): ", 0, 3);
        return op;        
    }
        
    public static String pause(){
        System.out.print("Type <intro> to continue: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
    
    public static String pause(String msg){
        System.out.print(msg);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    public static int testInteger(String message, int min, int max) {
        boolean exit=false;
        int intOk=0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(message);
                intOk=sc.nextInt();
                if (intOk>=min && intOk<=max) {
                    exit=true;
                } else {
                    System.out.println("Numbers only between " + min + " and " + max + "!");
                }
            } catch (Exception e) {
                System.out.println("Invalid Option!");
            } finally {
                sc.nextLine(); // Vacío buffer scanner
            }
        } while (!exit);
        return intOk;
    }    
    
    public static String testString(String mensaje, int numCaracteresMax) {
        boolean salir=false;
        String strOk="";
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                strOk=sc.next();
                if (strOk.length()<=numCaracteresMax) {
                    salir=true;
                } else {
                    System.out.println("Maximum number of characters is " + numCaracteresMax);
                }
            } catch (Exception e) {
                System.out.println("Invalid Option!");
            }
        } while (!salir);
        return strOk;
    }            
}
