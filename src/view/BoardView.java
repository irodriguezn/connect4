package view;

import tools.Constants;
import model.Board;

/**
 *
 * @author nacho
 */
public class BoardView {
    
    Board b;
    
    public BoardView(Board b) {
        this.b=b;
    }

    public void showBoard() {
        System.out.println("");
        for (int i = 1; i <= b.getRows(); i++) {
            for (int j = 1; j <= b.getCols(); j++) {
                switch (b.getElement(i,j)) {
                    case 0:
                        System.out.print(Constants.BLANK_CHAR + " ");
                        break;
                    case 1:
                        System.out.print(Constants.WHITE_CHAR + " ");
                        break;
                    case -1:
                        System.out.print(Constants.BLACK_CHAR + " ");
                        break;                        
                }
            }
            
            System.out.println("");
        }      
        for (int j = 1; j <= b.getCols(); j++) {
                System.out.print(cut(j) + " ");
        }
        System.out.println("\n");
        
    }
    
    private String cut(int n) {
        String num=String.valueOf(n);
        return num.substring(num.length()-1);
    }
}
